locals {
  org_service_url = "https://dev.azure.com/energix"

  project = {
    name               = "Artifact Storage"
    description        = "Artifact Storage for Energix"
    visibility         = "private"
    version_control    = "Git"
    work_item_template = "Agile"

    # Enable or disable the DevOps features below (enabled / disabled)
    features = {
      boards       = "disabled"
      repositories = "disabled"
      pipelines    = "disabled"
      testplans    = "disabled"
      artifacts    = "enabled"
    }
  }

  developers = concat(
    data.terraform_remote_state.aad.outputs.I-developer_users_new,
    data.terraform_remote_state.aad.outputs.I-developer_users_existing)
}