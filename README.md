Notes on the Terraform configuration
====================================

This Terraform configuration is not complete: the azuredevops provider does not
(yet) support the creation of feeds. See [Support the management of Azure
Artifacts feeds][1].

The feeds are currently manually created once the terraform configuration is
applied.

[1]: https://github.com/microsoft/terraform-provider-azuredevops/issues/50


NuGet feed creation
-------------------

* create new feed
    * name: energix-nuget
    * visibility: members of energix
    * upstream sources: include packages from common public sources
    * scope: project scope (Artifact Storage)
* fine-tune feed settings
    * feed details
        * deleted packages
            * enable 'hide deleted package versions'
        * package sharing
            * leave disabled
        * retention policies
            * leave disabled
    * permissions
        * see [Set up permissions - Azure Artifacts][2]
        * give the `Developers` group the `Owner` role
    * views
        * remove `Release` and `Prerelease` views
        * edit the remaining `Local` view
            * Access Permissions: All feeds and people in energix
            * enable 'make this the default view'
    * upstream sources
        * remove all upstream sources except the `NuGet Gallery`


NPM feed creation
-----------------

* create new feed
    * name: energix-npm
    * visibility: members of energix
    * upstream sources: include packages from common public sources
    * scope: project scope (Artifact Storage)
* fine-tune feed settings
    * feed details
        * deleted packages
            * enable 'hide deleted package versions'
        * package sharing
            * leave disabled
        * retention policies
            * leave disabled
    * permissions
        * see [Set up permissions - Azure Artifacts][2]
        * give the `Developers` group the `Owner` role
    * views
        * remove `Release` and `Prerelease` views
        * edit the remaining `Local` view
            * Access Permissions: All feeds and people in energix
            * enable 'make this the default view'
    * upstream sources
        * remove all upstream sources except the `npmjs`


[2]: https://docs.microsoft.com/en-us/azure/devops/artifacts/feeds/feed-permissions?view=azure-devops


Updating the configuration with Terraform
=========================================


Prerequisites
-------------

Terraform installed.

Create a new file inside the directory called `terraform.tfvars` with the following content:

```
personal_access_token = {your_personal_access_token_generated_on_devops}
```

Here is an example of an expected content for the file:

```
personal_access_token = "5ewen35lq4eqt3wwun4wkvvg2ig7fccuthk6hteoa5ql5niubmbq"
```

Note that you have to create the personal access token yourself.  Give the
personal access token full access.


Execution
---------

Run Powershell and change directory to this one

```shell
terraform init      # to download the provider
terraform fmt       # to format source files
terraform validate  # to check that the structure is OK
terraform apply     # to run, terraform.tfvars is used automatically
   # Type "yes" when prompted. 
# Check your created infrastructure at the organization's url
```


Optional:

```shell
terraform destroy # to remove all the created infrastructure
```

Using the Azure Artifact feeds
==============================

NuGet feed
----------

To use the NuGet feed, the preferable way is to provide a `NuGet.Config` file in
the root of the project.  This file should have the following contents:

```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <packageSources>
    <clear />
    <add key="energix-nuget"
         value="https://pkgs.dev.azure.com/energix/bf16b9cd-1e8e-4c7d-bca7-eb6551db6ac8/_packaging/energix-nuget/nuget/v3/index.json" />
  </packageSources>
</configuration>
```

It limits the nuget feed available for the project to `energix-nuget`.  The feed
itself has `nuget.org` as an upstream source, which means that packages
available on the standard source are also available through `energix-nuget`.

This `NuGet.Config` file should be added in source control.

### Authentication

Do note that the feed requires authentication.  This authentication can be done
through your IDE.  Alternatively, one can generate a personal access token and
use this as a password to authenticate against the feed.

To do this, one can create a `NuGet.Config` file near the root of the file
system.  It should be in a parent folder of the source code where the
`NuGet.Config` file with the `energix-nuget` repo is located.  That file should
contain something like the following:

```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <packageSourceCredentials>
    <energix-nuget>
      <add key="Username"
           value="ruben_vandeginste@peoplewarenv.onmicrosoft.com" />
      <add key="ClearTextPassword"
           value="76ffk5c6eq36z4e2rh4ute5sqhfnc65ifz6eejedsvslc2h2mhwa" />
    </energix-nuget>
  </packageSourceCredentials>
</configuration>
```
It should contain your username on Azure Devops as the username and the
generated personal access token as a password.  The PAT should have the
permission: 'Packaging: read, write & manage'.


NPM feed
--------

To use the NPM feed, a `.npmrc` file should be created in the root of the
project that contains the url of the registry to be used: the `energix-npm`
feed.  This file should have the following contents:

```ini
registry=https://pkgs.dev.azure.com/energix/bf16b9cd-1e8e-4c7d-bca7-eb6551db6ac8/_packaging/energix-npm/npm/registry/
always-auth=true
```

The NPM feed has the standard NPM feed as an upstream source, which means that
packages available on the standard source are also available through
`energix-npm`.

The `.npmrc` file should be added in source control.


### Authentication

Do note that the feed requires authentication.  On _Windows_, this
authentication can be done using the following tool:

```shell
vsts-npm-auth -config .npmrc
```

`vsts-npm-auth` is available on the standard NPM feed.  Note that it can be
installed as a global tool using `npm install -g vsts-npm-auth`.  Ensure that
this command is executed from a folder that does not override the standard NPM
feed!

Alternatively, one can generate a personal access token and use this as a
password to authenticate against the feed.  This works on all platforms.

To do this, one must create a user `.npmrc` file and add the following content
to it (or append to it if the file already exists):

```
; begin auth token
//pkgs.dev.azure.com/energix/bf16b9cd-1e8e-4c7d-bca7-eb6551db6ac8/_packaging/energix-npm/npm/registry/:username=energix
//pkgs.dev.azure.com/energix/bf16b9cd-1e8e-4c7d-bca7-eb6551db6ac8/_packaging/energix-npm/npm/registry/:_password=[BASE64_ENCODED_PERSONAL_ACCESS_TOKEN]
//pkgs.dev.azure.com/energix/bf16b9cd-1e8e-4c7d-bca7-eb6551db6ac8/_packaging/energix-npm/npm/registry/:email=npm requires email to be set but doesn't use the value
//pkgs.dev.azure.com/energix/bf16b9cd-1e8e-4c7d-bca7-eb6551db6ac8/_packaging/energix-npm/npm/:username=energix
//pkgs.dev.azure.com/energix/bf16b9cd-1e8e-4c7d-bca7-eb6551db6ac8/_packaging/energix-npm/npm/:_password=[BASE64_ENCODED_PERSONAL_ACCESS_TOKEN]
//pkgs.dev.azure.com/energix/bf16b9cd-1e8e-4c7d-bca7-eb6551db6ac8/_packaging/energix-npm/npm/:email=npm requires email to be set but doesn't use the value
; end auth token
```

The `base64 encoded personal access token` is a base64-encoded version of a
personal access token that must be generated by the user and that must have the
permission: 'Packaging: read, write & manage'.
