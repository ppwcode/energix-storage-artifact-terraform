terraform {
  required_providers {
    azuredevops = {
      source  = "microsoft/azuredevops"
      version = "=0.1.8"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.98.0"
    }
  }
}

provider "azuredevops" {
  org_service_url       = local.org_service_url
  personal_access_token = var.personal_access_token
}

provider "azurerm" {
  features {}

  subscription_id = "098ad191-f2fd-4240-9a7c-e269ab8394c0"
}

# project "artifact storage"
resource "azuredevops_project" "artifact_storage" {
  name               = local.project.name
  description        = local.project.description
  visibility         = local.project.visibility
  version_control    = local.project.version_control
  work_item_template = local.project.work_item_template

  # Enable or disable the DevOps features below (enabled / disabled)
  features = {
    "boards"       = local.project.features.boards
    "repositories" = local.project.features.repositories
    "pipelines"    = local.project.features.pipelines
    "testplans"    = local.project.features.testplans
    "artifacts"    = local.project.features.artifacts
  }
}

# all developers must be linked to the organization
resource "azuredevops_user_entitlement" "users" {
  for_each             = toset(local.developers)
  principal_name       = each.key
  account_license_type = "none"
  licensing_source     = "msdn"
}

# 'developers' group defined on the level of the organization
resource "azuredevops_group" "developers" {
  scope        = azuredevops_project.artifact_storage.id
  display_name = "Developers"
  description  = "Developers"

  members = [for o in azuredevops_user_entitlement.users : o.descriptor]
}

# assign project permissions to the 'developers' group
resource "azuredevops_project_permissions" "permissions" {
  project_id = azuredevops_project.artifact_storage.id
  principal  = azuredevops_group.developers.id
  permissions = {
    GENERIC_READ      = "Allow"
    GENERIC_WRITE     = "Allow"
    MANAGE_PROPERTIES = "Allow"
  }
}

# find the built-in group 'project collection administrators'
data "azuredevops_group" "administrators" {
  name = "Project Collection Administrators"
}

# add each developer to the administrators group
resource "azuredevops_group_membership" "developers_administrators_membership" {
  group = data.azuredevops_group.administrators.descriptor
  members = [for o in azuredevops_user_entitlement.users : o.descriptor]
}
